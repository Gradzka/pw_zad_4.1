#include <Windows.h>
#include "commctrl.h"
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>
#include "stdlib.h"
#include "fstream"
#include <omp.h> 

using namespace std;

#pragma comment(lib, "comctl32.lib")
#pragma warning (disable:4996)

LPSTR NazwaKlasy = "Klasa Okienka";
MSG Komunikat; //globalna zmienna do przechowywania komunikatow
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND g_hPrzycisk_1, g_hPrzycisk_2, hStatic_1, hStatic_2, hStatic_3, hStatic_4, hStatic_5, hStatic_6, hStatic_7, hStatic_8, hStatic_9, hStatic_10, h_Text;
volatile HWND hwnd;
HWND hProgressBar;

volatile bool stan_g_hPrzycisk_1 = 0;
fstream file;

//------------------------------------------------------------------------
//					 KLASA OKNA APLIKACJI - Tworzenie okna 
//------------------------------------------------------------------------
void application_window(WNDCLASSEX &wc,HINSTANCE hInstance){
	wc.cbSize = sizeof(WNDCLASSEX);					//rozmiar struktury w bajtach
	wc.style = 0;									//style klasy
	wc.lpfnWndProc = WndProc;						//wsk. do procedury oblugujacej okno
	wc.cbClsExtra = 0;								//dodatkowe bajty pamieci dla klasy
	wc.cbWndExtra = 0;								//j.w.
	wc.hInstance = hInstance;						//identyfikator aplikacji, ktora ma byc wlascicielem okna
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);		//ikonka okna
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);		//kursor myszy
	wc.hbrBackground = (HBRUSH)5;					//tlo ok
	wc.lpszMenuName = NULL;
	wc.lpszClassName = NazwaKlasy;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
}
void creation_window_application(HINSTANCE hInstance){
	RECT desktop;
	int horizontal = 0, vertical = 0, width = 420, height = 235;
	const HWND hDesktop = GetDesktopWindow();	// Get a handle to the desktop window
	GetWindowRect(hDesktop, &desktop);			//Get the size of screen to the variable desktop
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,		//rozszerzone parametry okna, 0 oznacza ich brak
		NazwaKlasy,								//adres tablicy char z nazwa klasy
		"GENERATOR",							//adres lancucha z napisem tytulowym okna
		WS_OVERLAPPEDWINDOW,					//styl okna
		horizontal / 2 - width / 2,				//wspolrzedna X lewego gornego naroznika okna
		vertical / 2 - height / 2,				//wspolrzedna Y --||--
		width,									//szerokosc okna w pikselach, moze byc w CW_USEDEFAULT
		height,									//wysokosc okna w pikselach, moze byc w CW_USEDEFAULT
		NULL,									//uchwyt okna rodzicielskiego, jesli istnieje
		NULL,									//uchyt menu dla naszego okna
		hInstance,								//uchwyt programu, ktory korzysta z okna
		NULL);									//adres dodatkowych danych
}
//------------------------------------------------------------------------
//									 CZAS
//------------------------------------------------------------------------
double start_time,pause_time_start=0,pause_time_end=0,pause_time_all=0,end_time;
//------------------------------------------------------------------------
//									 SILNIA 
//------------------------------------------------------------------------
int silnia(int n)
{
	if (n == 0|| n == 1) return 1;
	else return n*silnia(n - 1);
}
//------------------------------------------------------------------------
//							 GENEROWANIE PERMUTACJI  
//------------------------------------------------------------------------
int *Perm_Table;
volatile int nr_permutacji = 0;
int level;
void generator_permutacji(int *Table, int n, int k)
{
	level = level + 1; 
	Table[k] = level;
	if (level == n)
	{
		if (Table != 0)
		{
			if (file.is_open()){
				nr_permutacji += 1;
				for (int i = 0; i < n; i++){
				file << (Table[i] - 1);
				//printf("%s", Table[i] - 1);
			}
				file << endl;
				//printf("\n");
			}
		}
	}
	else
		for (int i = 0; i < n; i++)
			if (Table[i] == 0)
				generator_permutacji(Table, n, i);
	level = level - 1; 
	Table[k] = 0;
}
//------------------------------------------------------------------------
//					PROGRESS BAR, BUTTON, STATIC BUTTON
//------------------------------------------------------------------------
void Progress_Bar(HINSTANCE hInstance){
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icc.dwICC = ICC_BAR_CLASSES; // toolbary, statusbary i tooltipy
	InitCommonControlsEx(&icc);

	hProgressBar = CreateWindowEx(0, PROGRESS_CLASS, NULL, WS_CHILD | WS_VISIBLE | PBS_SMOOTH,
		30, 93, 240, 15, hwnd, (HMENU)7, hInstance, NULL);
}
void Button(HINSTANCE hInstance){
	g_hPrzycisk_1 = CreateWindow("BUTTON", "START / STOP", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		190, 20, 105, 30, hwnd, (HMENU)1, hInstance, NULL);

	g_hPrzycisk_2 = CreateWindowEx(0, "BUTTON", "PAUSE", WS_CHILD | WS_VISIBLE,
		310, 20, 60, 30, hwnd, (HMENU)2, hInstance, NULL);
}
void Static_Button(HINSTANCE hInstance){
	hStatic_1 = CreateWindowEx(0, "STATIC", "n", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 30, 20, 25, 30, hwnd, NULL, hInstance, NULL);
	h_Text = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 65, 22, 80, 25, hwnd, NULL, hInstance, NULL);
	hStatic_3 = CreateWindowEx(0, "STATIC", "n!", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 30, 55, 30, 30, hwnd, NULL, hInstance, NULL);
	hStatic_4 = CreateWindowEx(0, "STATIC", "", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 65, 55, 120, 30, hwnd, (HMENU)4, hInstance, NULL);
	hStatic_5 = CreateWindowEx(0, "STATIC", "CZAS OD POCZ�TKU OBLICZE� [s]: ", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 30, 115, 260, 30, hwnd, NULL, hInstance, NULL);
	hStatic_6 = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 293, 115, 80, 30, hwnd, (HMENU)8, hInstance, NULL);
	hStatic_7 = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 293, 85, 30, 30, hwnd, (HMENU)6, hInstance, NULL);
	hStatic_8 = CreateWindowEx(0, "STATIC", "%", WS_CHILD | WS_VISIBLE | SS_CENTER | SS_CENTERIMAGE, 320, 85, 20, 30, hwnd, NULL, hInstance, NULL);
	hStatic_9 = CreateWindowEx(0, "STATIC", "CZAS DO ZAKO�CZENIA OBLICZE� [s]: ", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 30, 150, 260, 30, hwnd, NULL, hInstance, NULL);
	hStatic_10 = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_LEFT | SS_CENTERIMAGE, 293, 150, 80, 30, hwnd, (HMENU)9, hInstance, NULL);
}
//------------------------------------------------------------------------
//									 BUFOR 
//------------------------------------------------------------------------
LPSTR Bufor;
int Bufor_wpisany = 0;
DWORD dlugosc_Bufora = 0;
//------------------------------------------------------------------------
//							FUNKCJE THERADS  
//------------------------------------------------------------------------
DWORD dwThreadId_1, dwThreadId_2;
HANDLE hThread_1, hThread_2;
bool PAUSE = 0;

DWORD WINAPI ThreadProcPermut(LPVOID lpParameter)
{
	
	//------------------ PERMUTACJE ------------------
	level = -1;
	// ----------------- PERM TABLE na 0 -------------
	Perm_Table = new int[Bufor_wpisany];
	for (int i = 0; i < Bufor_wpisany; i++)
		Perm_Table[i] = 0;
	// -----------------------------------------------
	pause_time_start = 0; pause_time_end = 0; pause_time_all = 0;
	start_time = omp_get_wtime();
	generator_permutacji(Perm_Table, Bufor_wpisany, 0);
	stan_g_hPrzycisk_1 = 0;
	file.close();
	delete Perm_Table;
	GlobalFree(Bufor);
	CloseHandle(hThread_1);
	CloseHandle(hThread_2);
	return 0;
}
DWORD WINAPI ThreadProcSupervisor(LPVOID lpParameter)
{
	int procent_wykonania = 0;
	double czas_do_zakonczenia = 0;
	string s;
	//-------------------------  WHILE  ---------------------------
	while (nr_permutacji<silnia(Bufor_wpisany)){
		procent_wykonania = nr_permutacji*100 / silnia(Bufor_wpisany); //Bufor_wpisany - np. n=6
		
		SetDlgItemText(hwnd, 6, itoa(procent_wykonania, Bufor, 10));
		SendMessage(hProgressBar, PBM_SETPOS, (WPARAM)procent_wykonania, 0);
		//-----------------   ZEGAR  -------------------------------
		
		end_time = omp_get_wtime();
		s = to_string(end_time-start_time-pause_time_all);
		SetDlgItemText(hwnd, 8, s.c_str()); // wylasnie czasu wykonania

		//-------------------- DO ZAKONCZENIA ---------------------
		czas_do_zakonczenia = (100 * (end_time - start_time - pause_time_all)) / (nr_permutacji * 100 / silnia(Bufor_wpisany)) - (end_time - start_time - pause_time_all);

		s = to_string(czas_do_zakonczenia);
		if (procent_wykonania==0)
		SetDlgItemText(hwnd, 9, "0.000000");
		else
		SetDlgItemText(hwnd, 9, s.c_str()); // wyslanie przewidzianego do zakonczenia
		//----------------------------------------------------------
		Sleep(10);
	}
	//---------------------------------------------------------------
	procent_wykonania = nr_permutacji*100 / silnia(Bufor_wpisany);
	
	SetDlgItemText(hwnd, 6, itoa(procent_wykonania, Bufor, 10));
	SendMessage(hProgressBar, PBM_SETPOS, (WPARAM)procent_wykonania, 0);

	//-------------------- DO ZAKONCZENIA ---------------------
	czas_do_zakonczenia = (100 * (end_time - start_time - pause_time_all)) / (nr_permutacji * 100 / silnia(Bufor_wpisany)) - (end_time - start_time - pause_time_all);

	s = to_string(czas_do_zakonczenia);
	SetDlgItemText(hwnd, 9, s.c_str()); // wyslanie przewidzianego do zakonczenia
	MessageBox(hwnd, "Program zako�czono!", "STOP", MB_ICONINFORMATION);

	return 0;
}

//------------------------------------------------------------------------
//							 WINAPI MAIN  
//------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance	//uchwyt aplikacji
	, HINSTANCE hPrevInstance,			//uchyt poprzedniego wystapienia aplikacji, zawsze NULL
	LPSTR lpCmdLine,					//zawiera linie polecen, z jakiej zostal uruchomiony program, LPSTR synonim *char
	int nCmdShow)						//okresla stan okna programu
{
	//------------------------------------------------------------------------
	//						KLASA OKNA APLIKACJI - Rejestrowanie klasy okna
	//------------------------------------------------------------------------
	WNDCLASSEX wc;
	application_window(wc, hInstance);
	
	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Error", "Window Class",
		MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}
	creation_window_application(hInstance); // Tworzenie okna

	if (hwnd == NULL)
	{
		MessageBox(NULL, "Error", "Window Error", MB_ICONEXCLAMATION);
		return 1;
	}
	//------------------------------------------------------------------------
	//					PROGRESS BAR, BUTTON, STATIC BUTTON
	//------------------------------------------------------------------------
	Progress_Bar(hInstance);
	Button(hInstance);
	Static_Button(hInstance);

	ShowWindow(hwnd, nCmdShow);	//Pokazanie okna na ekranie
	UpdateWindow(hwnd);			//Uaktualniamy tresc okna
	//------------------------------------------------------------------------
	//					PETLA KOMUNIKATOW
	//------------------------------------------------------------------------
	while (GetMessage(&Komunikat, NULL, 0, 0))	
	{
		TranslateMessage(&Komunikat);
		DispatchMessage(&Komunikat);
	}
	return Komunikat.wParam;
}

// OBS�UGA ZDARZE�
LRESULT CALLBACK WndProc(HWND hwnd,		//uchwyt okna
						 UINT msg,		//kod wiadomosci
						 WPARAM wParam, //parametr 16-bitowy
						 LPARAM lParam)	//parametr 32-bitowy
{
	switch (msg)
	{
	case WM_CLOSE: //umozliwia zamkniecie okna
		if (PAUSE == 0){
		DestroyWindow(hwnd);
		}
		break;
	case WM_DESTROY:
		if (PAUSE == 0)
		{
			PostQuitMessage(0);
			GlobalFree(Bufor);
		}
		break;

	case WM_COMMAND:
		if ((HWND)lParam == g_hPrzycisk_1) //START/STOP
		{
			if (stan_g_hPrzycisk_1 == 0) 
			{ 
				dlugosc_Bufora = GetWindowTextLength(h_Text);
				Bufor = (LPSTR)GlobalAlloc(GPTR, dlugosc_Bufora + 1);
				GetWindowText(h_Text, Bufor, dlugosc_Bufora + 1);

					Bufor_wpisany = atoi(Bufor);
					if (atoi(Bufor) > 10 || atoi(Bufor) < 1) 
						MessageBox(hwnd, "n nalezy do przedzia�u (0,10>", "ERROR", MB_ICONINFORMATION);

					else {
						nr_permutacji = 0;
						MessageBox(hwnd,"Start programu!", "START", MB_ICONINFORMATION);
						stan_g_hPrzycisk_1 = 1;

						//------------ ZAPIS PERMUTACJI DO PLIKU ------------------
						file.open("permut.txt", ios::out);//plik tylko do wpisywania, nie ma -> utworzy, jest -> nadpisze
						if (file.is_open())
							{
								//------------------ THREAD ------------------
									hThread_1 = CreateThread(NULL, 0, ThreadProcPermut, 0, 0, &dwThreadId_1);
									if (hThread_1 == NULL) ExitProcess(3);
								//------------------------   THREAD SUPERVISOR  ---------------------------
									hThread_2 = CreateThread(NULL, 0, ThreadProcSupervisor, 0, 0, &dwThreadId_2);
									if (hThread_2 == NULL) ExitProcess(3);
							}
						//------------------------------------------------

						itoa(silnia(Bufor_wpisany), Bufor, 10); //bufor zwiera n!
						SetDlgItemText(hwnd, 4, Bufor);
				}
			}
			//------------------------------------------------------------------------
			//					WSTRZYMANIE WATKU
			//------------------------------------------------------------------------
			else if (stan_g_hPrzycisk_1==1 && PAUSE == 0) 
			{
				PAUSE = 0;
				stan_g_hPrzycisk_1 = 0;
				if (file.is_open())
					{
						file.close();
					}
				delete Perm_Table;
				
				TerminateThread(hThread_1,0);
				TerminateThread(hThread_2,0);
				CloseHandle(hThread_1);
				CloseHandle(hThread_2);

				GlobalFree(Bufor);
				MessageBox(hwnd, "W�tki zosta�y zatrzymane!", "STOP", MB_ICONINFORMATION);
			}
		}
		else if ((HWND)lParam == g_hPrzycisk_2 &&stan_g_hPrzycisk_1 == 1 && PAUSE ==0)
		{
			SuspendThread(hThread_1);
			SuspendThread(hThread_2);
			pause_time_start =omp_get_wtime();
			PAUSE = 1;
			MessageBox(hwnd, "Wstrzymano w�tki!\n\nAby kontynuowa�,\nwci�nij PAUSE!", "PAUSE", MB_ICONINFORMATION);
		}
		else if ((HWND)lParam == g_hPrzycisk_2 && PAUSE==1)
		{
			MessageBox(hwnd, "Kontynuacja programu", "PAUSE", MB_ICONINFORMATION);
			ResumeThread(hThread_1);
			ResumeThread(hThread_2);
			pause_time_end = omp_get_wtime();
			pause_time_all = pause_time_all + (pause_time_end - pause_time_start);
			PAUSE = 0;	
		}
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}